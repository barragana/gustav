import intersection from 'lodash.intersection';
import difference from 'lodash.difference';

export function areAllCircleVendorsSelected (selectedVendors, circleVendors) {
  return selectedVendors.length >= circleVendors.length &&
  (intersection(selectedVendors, circleVendors) || []).length === circleVendors.length
}

export function prepareCirclesGroups (selectedVendors, circles) {
  return circles.reduce((res, c) => {
    if (areAllCircleVendorsSelected(selectedVendors, c.vendors)) {
      res.circlesGroups.push({
        group: 'circles',
        id: c.id,
        icon: 'circle',
        label: c.name,
        counter: c.vendors.length,
      });
      res.leftSelectedVendors = difference(res.leftSelectedVendors, c.vendors);
    }
    return res;
  }, { leftSelectedVendors: [...selectedVendors], circlesGroups: [] });
}
