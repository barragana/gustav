const { prepareCirclesGroups } = require("./utils");

const selectedVendors = ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M", "9LGKwGMYsszempFkmGnqJG", "JhwGAkLqZxicfNzDya2WMe"];
const circles = [{"id":"yvxPhVzItTgCjZP9HieyH9","name":"Sample Circle","vendors":["GnhBY9p52xUZFhzKqmAyTb","eGuLCNzrSSoj9p5tPHuFwU","uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","G5E7jz3bydEhHbxfzKTv8M","9LGKwGMYsszempFkmGnqJG","JhwGAkLqZxicfNzDya2WMe"]},{"id":"PadPa7JHRILRDMxYfGexZp","name":"Trusted Vendors Circle","vendors":["uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","RXcHtVBGU3dUTFPxKDgAGG","dqLyXFARAzTfQo2pqgAsGf","UAjB2oexqqcvPfxst4d8TV","UcqD4FeXKj7ZjeDZn9AppE","5dCqeQB2aWEqXcxrRwjL4A","4gYN2aWSofnEQFubdUrbHm","9LGKwGMYsszempFkmGnqJG","Sab8DBZzQCuba4yNeM3xVj","gpRgmdjqcYzj5Y9sdxh7MY","o2PpLBp66xuLDC4n98o2na","snhjbAAvZiPUuM6aocqDwC","RbjMxNeduY76Q6iJAUimt4","Rym5yiSUnuPnEZbHmiZWvd","gu7nruAjdnzBQZV3MQSpqT","cxKJL9bR3doh2uVZtEJEvn","JhwGAkLqZxicfNzDya2WMe"]}];

const validCirclesGroups = {
  leftSelectedVendors: [],
  circlesGroups: [{ group: 'circles', id: 'yvxPhVzItTgCjZP9HieyH9', icon: 'circle', label: 'Sample Circle', counter: 7 }],
};

describe('Utilities functions', () => {
  describe('prepareCirclesGroups should', () => {
    test('return circle group and left selected vendors', () => {
      const circlesGroups = prepareCirclesGroups(selectedVendors, circles);
      expect(circlesGroups).toEqual(validCirclesGroups);
    });
  });
});
