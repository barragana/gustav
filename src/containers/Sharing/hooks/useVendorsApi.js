import { useState, useEffect } from 'react';

import api from '../../../api';

function useVendorsApi() {
  const [result, setResult] = useState(null);

  useEffect(() => {
    Promise.all([
      api('/vendors'),
      api('/circles'),
    ]).then(([ vendors, circles ]) => setResult({ ...vendors, ...circles }));
  }, []);

  return result;
}

export default useVendorsApi;
