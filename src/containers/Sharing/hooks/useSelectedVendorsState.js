import { useEffect, useReducer } from "react";

const initialState = [];

export function reducer(state, action) {
  switch (action.type) {
    case 'CLEAR_ALL':
      return initialState;

    case 'SELECT_VENDOR':
      return [...state].concat(action.selectedVendor);

    case 'UNSELECT_VENDOR': {
      const vendorIndex = state.findIndex(id => id === action.selectedVendor);
      const nextState = [...state];
      nextState.splice(vendorIndex, 1);
      return nextState;
    }

    default:
      throw new Error();
  }
}



function useSelectedVendors(scope) {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    dispatch({ type: 'CLEAR_ALL' });
  }, [scope]);

  return [state, dispatch];
}

export default useSelectedVendors;
