import { useEffect, useReducer } from 'react';

import useVendorsApi from './useVendorsApi';

const initialState = {
  scopeGroups: {
    global: [{
      list: [{
        group: 'global',
        label: 'Global',
        id: 'global',
        counter: '',
        props: { heading: true, bold: true }
      }],
    }],
    vendors: [],
    internal: [{
      group: 'internal',
      list: [{
        group: 'internal',
        label: 'Internal',
        id: 'internal',
        counter: '',
        props: { heading: true, bold: true }
      }],
    }],
  },
  vendors: [],
  circles: [],
  vendorsMap: {},
};

export function reducer(state, action) {
  switch (action.type) {
    case 'SET_VENDORS_GROUPS':
      return {
        ...state,
        scopeGroups: {
          ...state.scopeGroups,
          vendors: action.vendors,
        },
      };
    case 'SET_VENDORS':
      return {
        ...state,
        vendors: action.vendors,
      };
    case 'SET_CIRCLES':
      return {
        ...state,
        circles: action.circles,
      };
    case 'SET_VENDORS_MAP':
      return {
        ...state,
        vendorsMap: action.vendorsMap,
      };
    case 'SET_CIRCLES_MAP':
      return {
        ...state,
        circlesMap: action.circlesMap,
      };
    default:
      throw new Error();
  }
}

function createMap (list, attributeName) {
  return list.reduce((res, item) => {
    res[item[attributeName]] = item;
    return res;
  }, {});
}

function useSharingState() {
  const vendorsGroups = useVendorsApi();
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    if (vendorsGroups) {
      const vendors = [{
        title: '',
        list: [{
          group: 'vendors',
          id: 'all_vendors',
          label: 'All vendor partners',
          counter: vendorsGroups.vendors.length,
          props: { heading: true, bold: true },
        }],
      }, {
        title: 'Circles',
        list: vendorsGroups.circles
          .map(({ id, name, vendors }) => ({
            id,
            group: 'circles',
            label: name,
            counter: vendors.length,
          })),
      }];

      dispatch({ type: 'SET_VENDORS_GROUPS', vendors });
      dispatch({ type: 'SET_VENDORS', vendors: vendorsGroups.vendors });
      dispatch({ type: 'SET_CIRCLES', circles: vendorsGroups.circles });

      dispatch({ type: 'SET_VENDORS_MAP', vendorsMap: createMap(vendorsGroups.vendors, 'id') });
      dispatch({ type: 'SET_CIRCLES_MAP', circlesMap: createMap(vendorsGroups.circles, 'id') });
    }
  }, [vendorsGroups]);

  return [state, dispatch];
}

export default useSharingState;
