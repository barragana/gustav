import { useReducer } from 'react';

const initialState = {
  scope: 'global',
  activeGroup: {
    group: 'global',
    id: 'global',
  },
};

export function reducer(state, action) {
  switch (action.type) {
    case 'SET_SCOPE':
      return {
        ...state,
        scope: action.scope,
        activeGroup: {
          id: action.scope,
        }
      };
    case 'SET_ACTIVE_GROUP':
      return {
        ...state,
        activeGroup: action.activeGroup,
      };
    default:
      throw new Error();
  }
}
function useScopeState() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return [state, dispatch];
}

export default useScopeState;
