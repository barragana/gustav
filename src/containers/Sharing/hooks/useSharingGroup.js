import { useState, useEffect } from "react";

function mapCirclesVendors (vendorsMap, circle) {
  return circle.vendors.map(id => (vendorsMap[id]));
}

function useSharingGroup(vendors, vendorsMap, circlesMap, activeGroup) {
  const [state, setState] = useState([]);

  useEffect(() => {
    if (activeGroup.group === 'vendors') {
      setState(vendors);
    }
  }, [vendors, activeGroup]);

  useEffect(() => {
    if (activeGroup.group === 'circles') {
      setState(mapCirclesVendors(vendorsMap, circlesMap[activeGroup.id]));
    }
  }, [vendorsMap, circlesMap, activeGroup]);

  return state;
}

export default useSharingGroup;
