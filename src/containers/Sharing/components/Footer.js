import React from 'react';
import styled from 'styled-components';

import Button from '../../../components/Button';
import Text from '../../../components/Text';

const StyledFooter = styled.div`
  border-top: 1px solid ${({ theme }) => theme.colors.t4};
  display: flex;
  justify-content: flex-end;
  padding: ${({ theme }) => theme.spacing.xl};
  width: 100%;
`;

function Footer ({ onClick, posting }) {
  return (
    <StyledFooter>
      <Button onClick={onClick} disabled={posting}>
        <Text color="white">
          { posting ? 'Publishing...' : 'Publish' }
        </Text>
      </Button>
    </StyledFooter>
  );
}

export default Footer;

