import React from 'react';
import styled from 'styled-components';

import Text from '../../../components/Text';
import SidebarListItem from './SidebarListItem';

export const Label = styled(Text)`
  color: ${({ theme }) => theme.colors.n6};
  height: 18px;
  padding: 0 ${({ theme }) => theme.spacing.l};
`;

function SidebarList ({ list, activeGroupId, onClick }) {
  return (
    <>
      {list.map(({ counter, label, id, group, logo, props, icon }) => (
        <SidebarListItem
          key={id}
          logo={logo}
          icon={icon}
          label={label}
          counter={counter}
          selected={id === activeGroupId}
          onClick={() => onClick({ counter, label, id, group })}
          {...props}
        />
      ))}
    </>
  );
}

export default SidebarList;
