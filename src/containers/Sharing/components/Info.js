import React from 'react';
import styled from 'styled-components';

import Text from '../../../components/Text';
import InfoBox from '../../../components/InfoBox';

const StyledInfo = styled.div`
  padding: ${({ theme }) => theme.spacing.l};
`;

const Highlighted = styled(Text)`
  margin: auto;
  outline: none;
  padding: 30px;
`;

function Info ({ link, text, onClick }) {
  return (
    <StyledInfo>
      <Highlighted size="l" bold color="t7">
        {link}
      </Highlighted>
      <InfoBox icon="warning" text={text} />
    </StyledInfo>
  );
}

export default Info;
