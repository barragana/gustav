import React from 'react';
import styled, { css } from 'styled-components';

import Icon from '../../../components/Icon/Icon';
import Logo from '../../../components/Logo';
import Text from '../../../components/Text';

import { withClickableStyle } from '../../../styles/utils';

const withSelectedStyle = ({ selected }) =>
  selected && css`
    border-left-color: ${({ theme }) => theme.colors.t5};
  `;

const withHeadingStyle = ({ heading, theme }) =>
  heading && css`
    margin-top: ${theme.spacing.l};
    margin-bottom: ${theme.spacing.xxl};
  `;

const Layout = styled.div`
  align-items: center;
  border-left: 3px solid transparent;
  display: flex;
  padding: 0 ${({ theme }) => `calc(${theme.spacing.l} - 3px)`};
  position: relative;
  width: 100%;
  ${withSelectedStyle};
  ${withClickableStyle};
  ${withHeadingStyle};
`;

const StyledIcon = styled(Icon)`
  margin-right: ${({ theme }) => theme.spacing.l};
`;

const StyleLogo = styled(Logo)`
  margin-right: ${({ theme }) => theme.spacing.l};
`;

function SidebarListItem ({ className, selected, label, counter, logo, onClick, bold, icon, heading }) {
  return (
    <Layout className={className} selected={selected} onClick={onClick} heading={heading}>
      {logo && <StyleLogo size="small" name={logo}/>}
      {icon && <StyledIcon icon={icon} />}
      <Text size="s" bold={selected || bold} textAlign="left" style={{ width: '100%' }}>
        {label}
      </Text>
      {counter && (
        <Text size="s" bold={selected || bold} textAlign="right" style={{ width: '36px' }}>
          {counter}
        </Text>
      )}
    </Layout>
  )
}

export default SidebarListItem;
