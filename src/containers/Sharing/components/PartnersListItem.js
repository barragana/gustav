import React from 'react';
import styled, { css } from 'styled-components';

import Logo from '../../../components/Logo';
import Text from '../../../components/Text';
import Checkbox from '../../../components/Checkbox';

const withSelectedStyled = ({ active, theme }) =>
  active && css`
    background-color: ${theme.colors.t3};
  `;

const Layout = styled.div`
  align-items: center;
  column-gap: ${({ theme }) => theme.spacing.xs};
  cursor: pointer;
  display: grid;
  grid-template-columns: auto 1fr auto;
  height: 50px;
  justify-items: start;
  padding: 0 ${({ theme }) => `${theme.spacing.xl}`};
  ${withSelectedStyled};
`;

function PartnersListItem ({ active, name, onClick }) {
  return (
    <Layout onClick={onClick} active={active}>
      <Logo size="large" name={name}/>
      <Text>{name}</Text>
      <Checkbox active={active} />
    </Layout>
  )
}

export default PartnersListItem;
