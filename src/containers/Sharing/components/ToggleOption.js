import React from 'react';
import styled, { css } from 'styled-components';

import Icon from '../../../components/Icon/Icon';
import Text from '../../../components/Text';

import theme from '../../../styles/theme';

const withActiveStyle = ({ active }) =>
  active && css`
    background-color: ${({ theme }) => theme.colors.selected.primary.background};;
    border-color: ${({ theme }) => theme.colors.selected.primary.background};
  `;

const withBaseStyle = () =>
  css`
    background-color: ${({ theme }) => theme.colors.default.secondary.background};
    border-color: ${({ theme }) => theme.colors.t4};
  `;

const StyledToggleOption = styled.button`
  align-items: center;
  border-style: solid;
  border-width: 1px;
  cursor: pointer;
  display: inline-flex;
  flex-grow: 1;
  font-size: 16px;
  justify-content: center;
  margin: 0px -1px;
  outline: none;
  text-align: center;
  width: 100%;

  ${props => withActiveStyle(props) || withBaseStyle()};

  &:first-child {
    border-radius: 3px 0 0 3px;
    margin: 0;
  }

  &:last-child {
    border-radius: 0 3px 3px 0;
    margin: 0;
  }
`;

const StyledIcon = styled(Icon)`
  margin-right: 8px;
`;

function ToggleOption({ option, active, onChange }) {
  const { id, label, icon } = option;

  const isActive = active === id;

  return (
    <StyledToggleOption active={isActive} onClick={() => onChange(id)}>
      <StyledIcon icon={icon} color={isActive ? 'white' : '#bac2cb'} size="m" />
      <Text
        size="l"
        color={isActive ?
            theme.colors.selected.primary.color :
            theme.colors.default.secondary.color}
      >
        { label }
      </Text>
    </StyledToggleOption>
  );
}

export default ToggleOption;
