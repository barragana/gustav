import React from 'react';
import styled from 'styled-components';

import Icon from '../../../components/Icon/Icon';
import Text from '../../../components/Text';

import theme from '../../../styles/theme';

const StyledHeader = styled.div`
  align-items: center;
  border-bottom: 1px solid ${({ theme }) => theme.colors.t4};
  column-gap: ${({ theme }) => theme.spacing.m};
  display: grid;
  grid-template-columns: 16px 1fr auto;
  height: 48px;
  padding: ${({ theme }) => `${theme.spacing.xs} ${theme.spacing.xl}`};
`;

function Header({ icon, label, children }) {
  return (
    <StyledHeader>
      <Icon size="l" icon={icon} color={theme.colors.t7} />
      <Text size="l" bold textAlign="left">
        {label}
      </Text>
      {children}
    </StyledHeader>
  );
}

export default Header;
