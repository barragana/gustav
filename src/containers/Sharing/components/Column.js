import React from 'react';
import styled, { css } from 'styled-components';

const withWhiteBGColor = () => css`
  background-color: white;
`;

const withGreyBGColor = ({ variation, theme }) =>
  variation === 'grey' && css`
    background-color: ${theme.colors.n2};
  `;

const withBorderStyle = ({ withBorder, theme }) =>
  withBorder && css`
    border-color: ${({ theme }) => theme.colors.t4};
    border-left-style: solid;
    border-right-style: solid;
    border-width: 1px;
  `;

const StyledColumn = styled.div`
  height: 100%;
  overflow: hidden;
  ${props => withGreyBGColor(props) || withWhiteBGColor()};
  ${withBorderStyle};
`;

function Column({ variation = 'white', withBorder, children }) {
  return (
    <StyledColumn variation={variation} withBorder={withBorder}>
      {children}
    </StyledColumn>
  );
}

export default Column;
