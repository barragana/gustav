import React from 'react';
import styled from 'styled-components';

import ToggleOption from './ToggleOption';

const StyledToggle = styled.div`
  background-color: #e8f1f2;
  border-bottom: 1px solid #d7e2e8;
  display: flex;
  height: 51px;
  padding: 10px 14px;
  width: 100%;
`;

function Toggle({ options = [], active, onChange }) {
  return (
    <StyledToggle>
      {options.map((option) =>
          <ToggleOption
            key={option.id}
            option={option}
            active={active}
            onChange={() => onChange(option.id)}
          />
      )}
    </StyledToggle>
  )
}

export default Toggle;
