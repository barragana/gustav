import React, { createContext } from "react";

import useScopeState from "./hooks/useScopeState";
import useSelectedVendors from "./hooks/useSelectedVendorsState";
import useSharingState from "./hooks/useSharingState";

export const SharingContext = createContext();

export function SharingProvider ({ children }) {
  const [state, dispatch] = useSharingState();
  const [scope, scopeDispatch] = useScopeState();
  const [selectedVendors, selectedVendorsDispatch] = useSelectedVendors(scope.scope);

  const value = {
    scopeContext: {
      state: scope,
      dispatch: scopeDispatch
    },
    sharingContext: {
      state,
      dispatch
    },
    selectedVendorsContext: {
      state: selectedVendors,
      dispatch: selectedVendorsDispatch,
    },
  };

  return (
    <SharingContext.Provider value={value}>
      {children}
    </SharingContext.Provider>
  );
}
