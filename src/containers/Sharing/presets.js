export const toggleOptions = [
  {id: 'global', label: 'Share globally on Gustav', icon: 'globe'},
  {id: 'vendors', label: 'Select vendors', icon: 'vendors'},
  {id: 'internal', label: 'Internal only', icon: 'internal'}
];

export const groupIcons = {
  global: 'globe',
  internal: 'internal',
  vendors: 'vendors',
  circles: 'circle',
};

export const scopesSettings = {
  global: {
    icon: 'globe',
    id: 'global',
    info: 'The job will shared globally. All vendors on Gustav will be able to see the job and submit candidates to it. Global jobs are reviewed by Gustav team. This may take up to 24 hours.',
    label: 'Global',
    link: 'Share job globally',
  },
  vendors: {
    icon: 'vendors',
    id: 'vendors',
    info: 'To share a job with selected vendors, please start by picking them in the left column.',
    label: 'Select vendors in the left column',
    link: 'Share job with selected vendors',
    name: 'All vendor partners',
  },
  internal: {
    icon: 'internal',
    id: 'internal',
    info: 'The job will only be visible to your internal team. You can share it to vendors or globally after it was published.',
    label: 'Internal',
    link: 'Share job internally',
  },
};
