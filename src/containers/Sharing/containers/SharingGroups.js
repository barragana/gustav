import React, { useContext, Fragment } from 'react';
import styled from 'styled-components';

import SidebarList, { Label } from '../components/SidebarList';

import { SharingContext } from '../SharingContext';

const Layout = styled.div`
  display: grid;
  grid-template-rows: repeat(2, auto);
  padding: ${({ theme }) => theme.spacing.xxs} 0;
  row-gap: ${({ theme }) => theme.spacing.s};
`;

function SharingGroups () {
  const { scopeContext, sharingContext } = useContext(SharingContext);
  const { scope, activeGroup } = scopeContext.state;
  const { scopeGroups } = sharingContext.state;

  const group = scopeGroups[scope];

  return (
    <Layout>
      {group.map(({ title, list }, i) => (
        <Fragment key={i}>
          {title && <Label textAlign="left" bold>{title}</Label>}
          <SidebarList
            activeGroupId={activeGroup.id}
            list={list}
            onClick={
              activeGroup => scopeContext.dispatch({
                type: 'SET_ACTIVE_GROUP',
                activeGroup,
              })}
          />
        </Fragment>
      ))}
    </Layout>
  )
}

export default SharingGroups;
