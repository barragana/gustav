import React, { useContext, useEffect, useRef } from 'react';
import styled from 'styled-components';

import Text from '../../../components/Text';
import SidebarList, { Label } from '../components/SidebarList';

import { SharingContext } from '../SharingContext';

import { prepareSelectedVendors } from './utils';
import { scopesSettings } from '../presets';

const Layout = styled.div`
  display: grid;
  grid-template-rows: 30px 1fr;
  padding: ${({ theme }) => theme.spacing.s} 0;
  row-gap: ${({ theme }) => theme.spacing.l};
  height: 100%;
`;

const Heading = styled.div`
  align-items: center;
  display: flex;
  justify-content: space-between;
  padding: 0 ${({ theme }) => theme.spacing.m};
  width: 100%;
`;

const SelectedLayout = styled.div`
  display: grid;
  grid-template-rows: 1fr;
  height: 100%;
  overflow-y: auto;;
`;

const ScrollPartners = styled.div`
  overflow-y: auto;
`;

const Wrapper = styled.div`
  align-items: center;
  display: grid;
  padding: ${({ theme }) => theme.spacing.l} 0;
  row-gap: ${({ theme }) => theme.spacing.s};
`;

function SelectedPartners () {
  const { selectedVendorsContext, sharingContext, scopeContext } = useContext(SharingContext);
  const { state: selectedVendors, dispatch } = selectedVendorsContext;
  const { vendorsMap, circles } = sharingContext.state;

  const isSingleSetting = ['global', 'internal'].includes(scopeContext.state.scope);
  const presentationList = isSingleSetting ?
    ([{ list: [scopesSettings[scopeContext.state.scope]] }]) :
    prepareSelectedVendors(
      selectedVendors,
      vendorsMap,
      circles,
    );

  const scrollRef = useRef();

  useEffect(() => {
    if (scrollRef) {
      scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
    }
  });

  return (
    <Layout>
      <Heading>
        <Text colors="n7" bold>Selected</Text>
        {!isSingleSetting && !!presentationList.length && (
          <Text onClick={() => dispatch({ type: 'CLEAR_ALL' })} bold colors="n7">
            Unselect all
          </Text>
        )}
      </Heading>
      <SelectedLayout>
        <ScrollPartners ref={scrollRef}>
          {!!presentationList.length && (
            presentationList.map(({ title, list }, i) => (
              <Wrapper key={title || i}>
                {title && <Label textAlign="left" bold>{title}</Label>}
                <SidebarList list={list} />
              </Wrapper>
            ))
          )}
        </ScrollPartners>
      </SelectedLayout>
    </Layout>
  );
}

export default SelectedPartners;
