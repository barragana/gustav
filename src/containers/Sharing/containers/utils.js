import { prepareCirclesGroups } from "../../../utils";

export function prepareSelectedVendors (selectedVendors, vendorsMap, circles) {
  if (!selectedVendors.length) return [];

  const groups = [];
  const { leftSelectedVendors, circlesGroups } =
    prepareCirclesGroups(selectedVendors, circles);

  if (circlesGroups.length) {
    groups.push({
      title: `Circles (${selectedVendors.length - leftSelectedVendors.length})`,
      list: circlesGroups,
    });
  }

  if (leftSelectedVendors.length) {
    groups.push({
      title: `Vendors (${leftSelectedVendors.length})`,
      list: leftSelectedVendors.map(id => ({
        ...vendorsMap[id],
        group: 'vendors',
        logo: vendorsMap[id].name,
        label: vendorsMap[id].name,
      }))
    });
  }

  return groups;
}
