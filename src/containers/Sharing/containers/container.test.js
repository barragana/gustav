const { prepareSelectedVendors } = require("./utils");

const selectedVendors = ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M", "9LGKwGMYsszempFkmGnqJG", "JhwGAkLqZxicfNzDya2WMe"];
const circles = [{"id":"yvxPhVzItTgCjZP9HieyH9","name":"Sample Circle","vendors":["GnhBY9p52xUZFhzKqmAyTb","eGuLCNzrSSoj9p5tPHuFwU","uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","G5E7jz3bydEhHbxfzKTv8M","9LGKwGMYsszempFkmGnqJG","JhwGAkLqZxicfNzDya2WMe"]},{"id":"PadPa7JHRILRDMxYfGexZp","name":"Trusted Vendors Circle","vendors":["uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","RXcHtVBGU3dUTFPxKDgAGG","dqLyXFARAzTfQo2pqgAsGf","UAjB2oexqqcvPfxst4d8TV","UcqD4FeXKj7ZjeDZn9AppE","5dCqeQB2aWEqXcxrRwjL4A","4gYN2aWSofnEQFubdUrbHm","9LGKwGMYsszempFkmGnqJG","Sab8DBZzQCuba4yNeM3xVj","gpRgmdjqcYzj5Y9sdxh7MY","o2PpLBp66xuLDC4n98o2na","snhjbAAvZiPUuM6aocqDwC","RbjMxNeduY76Q6iJAUimt4","Rym5yiSUnuPnEZbHmiZWvd","gu7nruAjdnzBQZV3MQSpqT","cxKJL9bR3doh2uVZtEJEvn","JhwGAkLqZxicfNzDya2WMe"]}];
const vendorsMap = {"GnhBY9p52xUZFhzKqmAyTb":{"id":"GnhBY9p52xUZFhzKqmAyTb","name":"Company 164"},"eGuLCNzrSSoj9p5tPHuFwU":{"id":"eGuLCNzrSSoj9p5tPHuFwU","name":"Company 134"},"oeEiq7HRNmB8ZgRoNQh8sg":{"id":"oeEiq7HRNmB8ZgRoNQh8sg","name":"Company 149"},"uwqqN4qMJ4RAx6WLyEBGAe":{"id":"uwqqN4qMJ4RAx6WLyEBGAe","name":"Company 179"},"NeZNQJxeMkCHCpiPK2XpDA":{"id":"NeZNQJxeMkCHCpiPK2XpDA","name":"Company 194"},"RXcHtVBGU3dUTFPxKDgAGG":{"id":"RXcHtVBGU3dUTFPxKDgAGG","name":"Company 209"},"uUx9Cyjc7VSsK5QnQ9fTLS":{"id":"uUx9Cyjc7VSsK5QnQ9fTLS","name":"Company 224"},"dqLyXFARAzTfQo2pqgAsGf":{"id":"dqLyXFARAzTfQo2pqgAsGf","name":"Company 239"},"UAjB2oexqqcvPfxst4d8TV":{"id":"UAjB2oexqqcvPfxst4d8TV","name":"Company 254"},"UcqD4FeXKj7ZjeDZn9AppE":{"id":"UcqD4FeXKj7ZjeDZn9AppE","name":"Company 269"},"5dCqeQB2aWEqXcxrRwjL4A":{"id":"5dCqeQB2aWEqXcxrRwjL4A","name":"Company 299"},"Vukt2rxY2RRc4oiVa7bnRS":{"id":"Vukt2rxY2RRc4oiVa7bnRS","name":"Company 329"},"tRdjMPZzsjXNDPoRMJHZ8c":{"id":"tRdjMPZzsjXNDPoRMJHZ8c","name":"Company 284"},"4gYN2aWSofnEQFubdUrbHm":{"id":"4gYN2aWSofnEQFubdUrbHm","name":"Company 314"},"9LGKwGMYsszempFkmGnqJG":{"id":"9LGKwGMYsszempFkmGnqJG","name":"Company 344"},"Sab8DBZzQCuba4yNeM3xVj":{"id":"Sab8DBZzQCuba4yNeM3xVj","name":"Company 359"},"gpRgmdjqcYzj5Y9sdxh7MY":{"id":"gpRgmdjqcYzj5Y9sdxh7MY","name":"Company 374"},"o2PpLBp66xuLDC4n98o2na":{"id":"o2PpLBp66xuLDC4n98o2na","name":"Company 389"},"G5E7jz3bydEhHbxfzKTv8M":{"id":"G5E7jz3bydEhHbxfzKTv8M","name":"Company 404"},"snhjbAAvZiPUuM6aocqDwC":{"id":"snhjbAAvZiPUuM6aocqDwC","name":"Company 419"},"RbjMxNeduY76Q6iJAUimt4":{"id":"RbjMxNeduY76Q6iJAUimt4","name":"Company 434"},"Rym5yiSUnuPnEZbHmiZWvd":{"id":"Rym5yiSUnuPnEZbHmiZWvd","name":"Company 449"},"gu7nruAjdnzBQZV3MQSpqT":{"id":"gu7nruAjdnzBQZV3MQSpqT","name":"Company 464"},"3rAkCBqpPZWWuDGk2xZwYU":{"id":"3rAkCBqpPZWWuDGk2xZwYU","name":"Company 479"},"cxKJL9bR3doh2uVZtEJEvn":{"id":"cxKJL9bR3doh2uVZtEJEvn","name":"Company 509"},"JhwGAkLqZxicfNzDya2WMe":{"id":"JhwGAkLqZxicfNzDya2WMe","name":"Company 494"}};
const validCirclesGroups = {
  leftSelectedVendors: [],
  circlesGroups: [{ group: 'circles', id: 'yvxPhVzItTgCjZP9HieyH9', icon: 'circle', label: 'Sample Circle', counter: 7 }],
};

describe('Sharing containers utilities functions', () => {
  describe('prepareSelectedVendors should', () => {
    test('return mix of circle group and vendors', () => {
      const groups = prepareSelectedVendors(selectedVendors, vendorsMap, circles);
      expect(groups).toEqual([{
        title: 'Circles (7)',
        list: validCirclesGroups.circlesGroups,
      }]);
    });
  });
});
