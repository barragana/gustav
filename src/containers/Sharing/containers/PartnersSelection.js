import React, { useContext } from 'react';
import styled from 'styled-components';
import difference from 'lodash.difference';

import Text from '../../../components/Text';
import Header from '../components/Header';
import Info from '../components/Info';
import PartnersListItem from '../components/PartnersListItem';
import Button from '../../../components/Button';

import { SharingContext } from '../SharingContext';

import useSharingGroup from '../hooks/useSharingGroup';

import { areAllCircleVendorsSelected } from '../../../utils';
import { groupIcons, scopesSettings } from '../presets';

const Layout = styled.div`
  display: grid;
  grid-template-rows: auto 1fr;
  height: 100%;
  row-gap: ${({ theme }) => theme.spacing.s};
`;

const PartnersList = styled.div`
  display: grid;
  grid-template-rows: 42px 1fr;
  height: 100%;
  overflow-y: auto;
`;

const ScrollPartners = styled.div`
  height: 100%;
  overflow-y: auto;
`;

const Label = styled(Text)`
  align-self: center;
  padding: 0 ${({ theme }) => `${theme.spacing.xl}`};
`;

function mergePartnerListConfig (scope, activeGroup) {
  return scopesSettings[activeGroup.id] || {
    ...scopesSettings[scope],
    ...activeGroup,
    icon: groupIcons[activeGroup.group],
  };
}

function PartnersSelection () {
  const {
    scopeContext,
    sharingContext,
    selectedVendorsContext,
  } = useContext(SharingContext);
  const { vendorsMap, circlesMap, vendors } = sharingContext.state;
  const { scope, activeGroup } = scopeContext.state;
  const { state: selectedVendors, dispatch } = selectedVendorsContext;

  const partnersList = useSharingGroup(vendors, vendorsMap, circlesMap, activeGroup);
  const partnersListConfig = mergePartnerListConfig(scope, activeGroup);

  return (
    <Layout>
      <Header
        icon={partnersListConfig.icon}
        label={partnersListConfig.label}
      >
      {partnersListConfig.group === 'circles' && (
          <Button
            disabled={areAllCircleVendorsSelected(selectedVendors, circlesMap[activeGroup.id].vendors)}
            onClick={() => dispatch({
              type: 'SELECT_VENDOR',
              selectedVendor: difference(circlesMap[activeGroup.id].vendors, selectedVendors),
            })}
          >
            <Text>Select entire circle</Text>
          </Button>
      )}
      </Header>
      {scope === activeGroup.id ? (
        <Info
          link={partnersListConfig.link}
          text={partnersListConfig.info}
          onClick={() => {}}
        />
      ) : (
        <PartnersList>
          <Label bold color="n6" textAlign="left">
            Vendors ({partnersListConfig.counter})
          </Label>
          <ScrollPartners>
            {partnersList.map(({ id, name }) => {
              const isActive = selectedVendors.includes(id);
              const handleClick = () => {
                dispatch({
                  type: isActive ? 'UNSELECT_VENDOR' : 'SELECT_VENDOR',
                  selectedVendor: id,
                });
              }

              return (
                <PartnersListItem
                  key={id}
                  id={id}
                  name={name}
                  active={isActive}
                  onClick={handleClick}
                />
              );
            })}
          </ScrollPartners>
        </PartnersList>
      )}
    </Layout>
  )
}

export default PartnersSelection;
