/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useState } from 'react';
import styled from 'styled-components';

import Column from './components/Column';
import Footer from './components/Footer';
import Toggle from './components/Toggle';

import SharingGroups from './containers/SharingGroups';
import PartnersSelection from './containers/PartnersSelection';
import SelectedPartners from './containers/SelectedPartners';

import { SharingContext } from './SharingContext';

import { toggleOptions } from './presets';
import { publishJob } from '../../actions';

const OuterLayout = styled.div`
  display: grid;
  grid-template-rows: auto minmax(250px, 1fr) 70px;
  height: 100vh;
`;

const InnerLayout = styled.div`
  display: grid;
  grid-template-columns: 280px auto 280px;
  height: 100%;
`;

function Sharing () {
  const {
    scopeContext,
    selectedVendorsContext,
    sharingContext,
  } = useContext(SharingContext);
  const selectedVendors = selectedVendorsContext.state;
  const { circles } = sharingContext.state;
  const { state: { scope }, dispatch } = scopeContext;

  const [state, setState] = useState({ posting: false })

  const handlePublishJob = () => {
    setState({ posting: true });
    publishJob(scope, selectedVendors, circles)
    .then(() => {
      setState({ posting: false });
    });
  }

  const handleToggleChange = selectedScope => {
    dispatch({ type: 'SET_SCOPE', scope: selectedScope })
  }

  return (
    <OuterLayout>
      <Toggle
        options={toggleOptions}
        active={scope}
        onChange={handleToggleChange}
      />
      <InnerLayout>
        <Column>
          <SharingGroups />
        </Column>
        <Column withBorder>
          <PartnersSelection />
        </Column>
        <Column variation="grey">
          <SelectedPartners />
        </Column>
      </InnerLayout>
      <Footer onClick={handlePublishJob} posting={state.posting} />
    </OuterLayout>
  );
}

export default Sharing;
