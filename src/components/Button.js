import styled from 'styled-components';

import Text from './Text';

import { withClickableStyle } from '../styles/utils';

const Button = styled.button`
  border: none;
  border-radius: 3px;
  background-color: ${({ theme }) => theme.colors.t7};
  padding: ${({ theme }) => `${theme.spacing.xs} ${theme.spacing.m}`};

  ${Text} {
    color: white;
  }

  &:disabled {
    background-color: ${({ theme }) => theme.colors.n4};
    ${Text} {
      color: ${({ theme }) => theme.colors.n6};
    }
  }

  ${withClickableStyle}
`;

export default Button;
