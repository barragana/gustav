import React from 'react';
import styled from 'styled-components';

import { ReactComponent as Check} from './check.svg';

import { colors } from '../styles/theme';

const Layout = styled.div`
  border-radius: 1px;
  border: solid 1px ${({ theme }) => theme.colors.n5};
  background-color: white;
  height: 16px;
  width: 16px;
`;

function Checkbox ({ active }) {
  return (
    <Layout>
      {active && <Check height="14" width="14" fill={colors.n9} />}
    </Layout>
  );
}

export default Checkbox;
