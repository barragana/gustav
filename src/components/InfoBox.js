import React from 'react';
import styled from 'styled-components';

import Icon from './Icon/Icon';
import Text from './Text';

import { colors } from '../styles/theme';

const StyledInfoBox = styled.div`
  background-color: ${({ theme }) => theme.colors.t3};
  border: 1px solid ${({ theme }) => theme.colors.t2};
  border-radius: 3px;
  display: flex;
  padding: ${({ theme }) => `${theme.spacing.l} ${theme.spacing.l} ${theme.spacing.m}`};
  ${({ theme }) => theme.fontSizes.m};
`;

const StyledIcon = styled(Icon)`
  align-self: flex-start;
  flex-shrink: 0;
  margin-right: ${({ theme }) => theme.spacing.s};
  width: 20px;
`;

function InfoBox({ icon, text }) {
  return (
    <StyledInfoBox>
      <StyledIcon icon={icon} color={colors.t7} />
      <Text>{ text }</Text>
    </StyledInfoBox>
  );
}

export default InfoBox;
