import React from 'react';
import styled, { css } from 'styled-components';

const withLargeSize = ({ size }) =>
  size === 'large' &&
  css `
    font-size: 18px;
    height: 30px;
    padding-top: 2px;
    width: 30px;
  `;

const withSmallSize = ({ size }) =>
  size === 'small' &&
  css `
    font-size: 10px;
    height: 18px;
    padding-top: 1px;
    width: 18px;
  `;

const StyledLogo = styled.div`
  box-sizing: border-box;
  border-radius: 1px;
  border: solid 1px ${({ theme }) => theme.colors.t4};
  color: white;
  text-align: center;
  ${props => withLargeSize(props) || withSmallSize(props)};
`;

function Logo({ className, name, size = "large" }) {
  const logoColors = [
    '#0B748C',
    '#0AAFC4',
    '#FF7656',
    '#FBAC9E',
    '#00A878',
    '#FABC3C',
    '#C391B9',
    '#8779B3',
    '#5A6EAE'
  ]

  const colorIndex = name.charCodeAt(0) % logoColors.length;
  const backgroundColor = logoColors[colorIndex];

  return (
    <StyledLogo className={className} style={{ backgroundColor }} size={size}>
      { name[0] }
    </StyledLogo>
  )
}

export default Logo;
