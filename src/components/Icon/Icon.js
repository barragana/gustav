import React from 'react';
import styled, { css } from 'styled-components';

import PATHS from './paths';

const SIZES = {
  s: '14px',
  m: '16px',
  l: '18px',
};

const withSize = ({ size }) =>
  size && css`
    height: ${SIZES[size]};
    width: ${SIZES[size]};
  `;

const StyledSvg = styled.svg`
  ${withSize};
`;

function Icon({ className, icon, color = "black", size = 'm' }) {
  return (
    <StyledSvg className={className} viewBox="0 0 1024 1024" size={size}>
      <path fill={color} color={color} d={PATHS[icon]}></path>
    </StyledSvg>
  )
}

export default Icon;
