import styled, { css } from 'styled-components';
import { withClickableStyle } from '../styles/utils';

const withBoldStyle = ({ bold }) =>
  bold && css`
    font-weight: 500;
  `;

const withTextAlignStyle = ({ textAlign }) =>
  textAlign && css`
    text-align: ${textAlign};
    justify-self: ${textAlign};
  `;

const withCustomColor = ({ color, theme }) =>
  color && css`
    color: ${theme.colors[color] || color};
  `;

const Text = styled.p`
  margin: 0;
  ${({ theme, size = 'm' }) => theme.fontSizes[size]};
  ${withBoldStyle};
  ${withTextAlignStyle};
  ${withCustomColor};
  ${withClickableStyle};
`;

export default Text;
