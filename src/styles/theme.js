import { createGlobalStyle } from 'styled-components';

export const colors = {
  t2: '#eceef1',
  t3: '#e8f1f2',
  t4: '#d7e2e8',
  t5: '#0aafc4',
  t7: '#0b748c',

  n2: '#fafbfc',
  n3: '#bac2cb',
  n4: '#dee1e5',
  n5: '#c1c7cd',
  n6: '#89949e',
  n7: '#4e5f6e',
  n9: '#13293d',
}

export const spacing = {
  xxs: '4px',
  xs: '8px',
  s: '10px',
  m: '12px',
  l: '14px',
  xl: '18px',
  xxl: '24px',
};

export const fontSizes = {
  s: {
    'font-size': '13px',
    'line-height': '1.08',
  },
  m: {
    'font-size': '14px',
  },
  l: {
    'font-size': '16px',
    'line-height': '1.5',
  },
}

const theme = {
  colors: {
    ...colors,
    selected: {
      primary: {
        background: colors.t5,
        color: 'white',
      },
      secondary: {
        background: colors.t3,
        color: colors.n9,
      },
    },
    default: {
      primary: {
        color: colors.n9,
      },
      secondary: {
        background: 'white',
        color: colors.n3,
      }
    }
  },
  fontSizes,
  spacing,
}

export const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    color: ${({ theme }) => theme.colors.n9};
  }
`;

export default theme;
