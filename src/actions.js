import request from "./api";
import { prepareCirclesGroups } from "./utils";

export function publishJob (scope, selectedVendors, circles) {
  const {
    leftSelectedVendors,
    circlesGroups,
  } = prepareCirclesGroups(selectedVendors, circles);

  const payload = {
    global: scope === 'global',
    internal: scope === 'internal',
    circles: circlesGroups.map(({ id }) => id),
    vendors: leftSelectedVendors,
  };

  return request('/jobs', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(payload),
  });
}
