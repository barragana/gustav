const { publishJob } = require("./actions");

jest.mock('./api', () =>
  (() => {
    return jest.fn();
  })(),
);

const request = require('./api');

describe('actions', () => {
  describe('publishJob should', () => {
    test('call request for global', () => {
      const scope = 'global';
      const selectedVendors = [];
      const circles = [];
      publishJob(scope, selectedVendors, circles);
      expect(request).toBeCalledWith('/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          global: true,
          internal: false,
          circles: [],
          vendors: [],
        }),
      });
    });

    test('call request for internal', () => {
      const scope = 'internal';
      const selectedVendors = [];
      const circles = [];
      publishJob(scope, selectedVendors, circles);
      expect(request).toBeCalledWith('/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          global: false,
          internal: true,
          circles: [],
          vendors: [],
        }),
      });
    });

    test('call request for vendors', () => {
      const scope = 'vendors';
      const selectedVendors = ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M"];
      const circles = [{"id":"yvxPhVzItTgCjZP9HieyH9","name":"Sample Circle","vendors":["GnhBY9p52xUZFhzKqmAyTb","eGuLCNzrSSoj9p5tPHuFwU","uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","G5E7jz3bydEhHbxfzKTv8M","9LGKwGMYsszempFkmGnqJG","JhwGAkLqZxicfNzDya2WMe"]},{"id":"PadPa7JHRILRDMxYfGexZp","name":"Trusted Vendors Circle","vendors":["uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","RXcHtVBGU3dUTFPxKDgAGG","dqLyXFARAzTfQo2pqgAsGf","UAjB2oexqqcvPfxst4d8TV","UcqD4FeXKj7ZjeDZn9AppE","5dCqeQB2aWEqXcxrRwjL4A","4gYN2aWSofnEQFubdUrbHm","9LGKwGMYsszempFkmGnqJG","Sab8DBZzQCuba4yNeM3xVj","gpRgmdjqcYzj5Y9sdxh7MY","o2PpLBp66xuLDC4n98o2na","snhjbAAvZiPUuM6aocqDwC","RbjMxNeduY76Q6iJAUimt4","Rym5yiSUnuPnEZbHmiZWvd","gu7nruAjdnzBQZV3MQSpqT","cxKJL9bR3doh2uVZtEJEvn","JhwGAkLqZxicfNzDya2WMe"]}];

      publishJob(scope, selectedVendors, circles);
      expect(request).toBeCalledWith('/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          global: false,
          internal: false,
          circles: [],
          vendors: ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M"],
        }),
      });
    });

    test('call request for vendors and circles', () => {
      const scope = 'vendors';
      const selectedVendors = ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M", "9LGKwGMYsszempFkmGnqJG", "JhwGAkLqZxicfNzDya2WMe", "UcqD4FeXKj7ZjeDZn9AppE"];
      const circles = [{"id":"yvxPhVzItTgCjZP9HieyH9","name":"Sample Circle","vendors":["GnhBY9p52xUZFhzKqmAyTb","eGuLCNzrSSoj9p5tPHuFwU","uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","G5E7jz3bydEhHbxfzKTv8M","9LGKwGMYsszempFkmGnqJG","JhwGAkLqZxicfNzDya2WMe"]},{"id":"PadPa7JHRILRDMxYfGexZp","name":"Trusted Vendors Circle","vendors":["uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","RXcHtVBGU3dUTFPxKDgAGG","dqLyXFARAzTfQo2pqgAsGf","UAjB2oexqqcvPfxst4d8TV","UcqD4FeXKj7ZjeDZn9AppE","5dCqeQB2aWEqXcxrRwjL4A","4gYN2aWSofnEQFubdUrbHm","9LGKwGMYsszempFkmGnqJG","Sab8DBZzQCuba4yNeM3xVj","gpRgmdjqcYzj5Y9sdxh7MY","o2PpLBp66xuLDC4n98o2na","snhjbAAvZiPUuM6aocqDwC","RbjMxNeduY76Q6iJAUimt4","Rym5yiSUnuPnEZbHmiZWvd","gu7nruAjdnzBQZV3MQSpqT","cxKJL9bR3doh2uVZtEJEvn","JhwGAkLqZxicfNzDya2WMe"]}];

      publishJob(scope, selectedVendors, circles);
      expect(request).toBeCalledWith('/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          global: false,
          internal: false,
          circles: ["yvxPhVzItTgCjZP9HieyH9"],
          vendors: ["UcqD4FeXKj7ZjeDZn9AppE"],
        }),
      });
    });

    test('call request for circles', () => {
      const scope = 'vendors';
      const selectedVendors = ["GnhBY9p52xUZFhzKqmAyTb", "eGuLCNzrSSoj9p5tPHuFwU", "uwqqN4qMJ4RAx6WLyEBGAe", "NeZNQJxeMkCHCpiPK2XpDA", "G5E7jz3bydEhHbxfzKTv8M", "9LGKwGMYsszempFkmGnqJG", "JhwGAkLqZxicfNzDya2WMe"];
      const circles = [{"id":"yvxPhVzItTgCjZP9HieyH9","name":"Sample Circle","vendors":["GnhBY9p52xUZFhzKqmAyTb","eGuLCNzrSSoj9p5tPHuFwU","uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","G5E7jz3bydEhHbxfzKTv8M","9LGKwGMYsszempFkmGnqJG","JhwGAkLqZxicfNzDya2WMe"]},{"id":"PadPa7JHRILRDMxYfGexZp","name":"Trusted Vendors Circle","vendors":["uwqqN4qMJ4RAx6WLyEBGAe","NeZNQJxeMkCHCpiPK2XpDA","RXcHtVBGU3dUTFPxKDgAGG","dqLyXFARAzTfQo2pqgAsGf","UAjB2oexqqcvPfxst4d8TV","UcqD4FeXKj7ZjeDZn9AppE","5dCqeQB2aWEqXcxrRwjL4A","4gYN2aWSofnEQFubdUrbHm","9LGKwGMYsszempFkmGnqJG","Sab8DBZzQCuba4yNeM3xVj","gpRgmdjqcYzj5Y9sdxh7MY","o2PpLBp66xuLDC4n98o2na","snhjbAAvZiPUuM6aocqDwC","RbjMxNeduY76Q6iJAUimt4","Rym5yiSUnuPnEZbHmiZWvd","gu7nruAjdnzBQZV3MQSpqT","cxKJL9bR3doh2uVZtEJEvn","JhwGAkLqZxicfNzDya2WMe"]}];

      publishJob(scope, selectedVendors, circles);
      expect(request).toBeCalledWith('/jobs', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          global: false,
          internal: false,
          circles: ["yvxPhVzItTgCjZP9HieyH9"],
          vendors: [],
        }),
      });
    });
  });
});
