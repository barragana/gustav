import React from 'react';
import { ThemeProvider } from 'styled-components';

import Sharing from './containers/Sharing/Sharing';
import { SharingProvider } from './containers/Sharing/SharingContext';

import theme, { GlobalStyle } from './styles/theme';
import './App.css';

function App() {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <div className="App">
        <SharingProvider>
          <Sharing />
        </SharingProvider>
      </div>
    </ThemeProvider>
  );
}

export default App;
